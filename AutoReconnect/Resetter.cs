﻿using System;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Threading;
using System.Timers;

namespace AutoReconnect
{
    public sealed class Resetter : IDisposable
    {
        private readonly object locker = new object();
        private readonly System.Timers.Timer timer;

        public Resetter()
        {
            this.timer = new System.Timers.Timer();
            this.timer.Elapsed += Timer_Elapsed;
            this.timer.AutoReset = true;
            this.timer.Interval = TimeSpan.FromSeconds(10).TotalMilliseconds;
        }

        public void Start()
        {
            Logger.Log("Starting resetter");

            this.timer.Enabled = true;

            this.CheckConnectivity();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.CheckConnectivity();
        }

        private void CheckConnectivity()
        {
            try
            {
                if (Monitor.TryEnter(this.locker))
                {
                    try
                    {
                        using (Ping ping = new Ping())
                        {
                            ping.Send(Constants.DefaultPingHost);
                        }
                    }
                    catch (PingException pingException)
                    {
                        Logger.Log("Internet connectivity failed");
                        Logger.Log(pingException.ToString());

                        this.ResetInternet();
                    }
                    catch (Exception exception)
                    {
                        Logger.Log(exception.ToString());
                    }
                }
            }
            finally
            {
                Monitor.Exit(this.locker);
            }           
        }

        private void ResetInternet()
        {
            Logger.Log("Resetting connection start");

            this.ExecCommand("ipconfig /release");
            this.ExecCommand("ipconfig /renew");
            this.ExecCommand("ipconfig /flushdns");

            Logger.Log("Resetting connection finished");
        }

        private void ExecCommand(string command)
        {
            Logger.Log($"ExecCommand={command}");

            var startInfo = new ProcessStartInfo();
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = $"/C {command}";

            var process = new Process();
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();
        }

        public void Dispose()
        {
            Logger.Log("Disposing resetter");

            this.timer.Dispose();
        }
    }
}
