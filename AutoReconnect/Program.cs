﻿using System;
using System.Diagnostics;
using System.Threading;

namespace AutoReconnect
{
    public static class Program
    {
        static void Main(string[] args)
        {
            if (!Environment.UserInteractive)
            {
                Logger.Log("Service mode");
                using (var resetter = new Resetter())
                {
                    resetter.Start();

                    while (true)
                    {
                        Thread.Sleep(1000);
                    }
                }
            }

            if (args.Length == 0)
            {
                Console.WriteLine("Usage:");
                Console.WriteLine("AutoReconnect.exe install            - installs service");
                Console.WriteLine("AutoReconnect.exe uninstall          - uninstalls service ");
                Console.WriteLine("AutoReconnect.exe run                - runs service in console");
                return;
            }

            if (args.Length > 0)
            {
                switch (args[0].ToLower().Trim())
                {
                    default:
                        Logger.Log("Unknown argument");
                        break;
                    case "install":
                        Logger.Log("Installing service...");
                        ExecNssm("install AutoReconnect AutoReconnect.exe");
                        ExecNssm("set AutoReconnect Start SERVICE_AUTO_START");
                        ExecNssm("start AutoReconnect");
                        break;
                    case "uninstall":
                        Logger.Log("Uninstalling service...");
                        ExecNssm("remove AutoReconnect confirm");
                        break;
                    case "run":
                        Logger.Log("User interactive mode");
                        using (var resetter = new Resetter())
                        {
                            resetter.Start();

                            Logger.Log("Press any key to stop the application");
                            Console.ReadKey();
                        }
                        break;
                }

                return;
            }

          
        }

        private static void ExecNssm(string command)
        {
            var directory = System.IO.Path.GetDirectoryName(typeof(Program).Assembly.Location);

            var startInfo = new ProcessStartInfo();
            startInfo.WorkingDirectory = directory;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.FileName = Environment.Is64BitProcess ? "nssm_x64.exe" : "nssm_x32.exe";
            startInfo.Arguments = command;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            var process = new Process();
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();
        }
    }
}
