﻿using System;
using System.IO;

namespace AutoReconnect
{
    public static class Logger
    {
        public static void Log(string message)
        {
            string toAppend = $"{DateTime.Now} {message}";

            try
            {
                Console.WriteLine(toAppend);

                var directory = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                        "AutoReconnect");

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                var path = Path.Combine(directory, "Log.txt");

                File.AppendAllText(path, toAppend + Environment.NewLine);
            }
            catch
            {
            }
        }
    }
}
